package dev.leandroerllet.silver.restart.service;

import de.themoep.minedown.MineDown;
import dev.leandroerllet.silver.restart.Restart;
import lombok.*;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;


public class RestartService {

    @Getter
    @Setter(AccessLevel.PRIVATE)
    private static boolean restarting;

    @Getter
    @Setter(AccessLevel.PRIVATE)
    private static String reason;

    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private static int stopTask;

    public static void restart(Integer time, String reason) {
        setReason(reason);
        setRestarting(true);
        val restartMessage = MineDown.parse(
                "\n&c[Alert] &eThe Server is restarting soon!" +
                        "\n&c[Alert] &eReason: &a" + StringUtils.capitalize(reason) +
                        "\n&c[Alert] &eU've " + time + " seconds to leave for the lobby! " +
                        "[Click](format=underline,bold color=green run_command=/evacuate show_text=&a/lobby)\n");
        Bukkit.getOnlinePlayers().forEach(player -> player.spigot().sendMessage(restartMessage));
        setStopTask(Bukkit.getScheduler().scheduleSyncDelayedTask(Restart.getInstance(), RestartService::safeStop, 20L * time));
    }

    @SneakyThrows
    public static void evacuate(Player player) {
        val outStream = new ByteArrayOutputStream();
        val out = new DataOutputStream(outStream);
        out.writeUTF("Connect");
        out.writeUTF("lobby");
        player.sendPluginMessage(Restart.getInstance(), "BungeeCord", outStream.toByteArray());
    }

    public static void cancelRestart() {
        Bukkit.getScheduler().cancelTask(getStopTask());
        setRestarting(false);

        val restartCancelledMessage = MineDown.parse(
                "\n&c[Alert] The restart was stopped!\n");
        Bukkit.getOnlinePlayers().forEach(player -> player.spigot().sendMessage(restartCancelledMessage));
    }




    private static void safeStop() {
        val kickMessage = MineDown.parse(
                "&eO servidor está reiniciando, voltamos em alguns minutos!" +
                        "\n&8Você foi enviado para o lobby");

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.spigot().sendMessage(kickMessage);
            evacuate(player);
        });

        Bukkit.getScheduler().scheduleSyncDelayedTask(Restart.getInstance(),() -> Restart.getInstance().getServer().shutdown(), 20 * 10);

    }
}
