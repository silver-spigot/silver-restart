package dev.leandroerllet.silver.restart.listener;

import dev.leandroerllet.silver.restart.service.RestartService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;


public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onJoin(PlayerLoginEvent e) {
        if (RestartService.isRestarting())
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER,
                    "\u00a7eThe Server is restarting, reason:: " + RestartService.getReason());
    }
}
