package dev.leandroerllet.silver.restart.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import dev.leandroerllet.silver.restart.service.RestartService;
import org.bukkit.entity.Player;
import de.themoep.minedown.MineDown;

@CommandAlias("restart|reiniciar")
public class RestartCommand extends BaseCommand {


    @Default
    @Description("restart the server")
    @Subcommand("restart")
    @CommandPermission("silver.restart.restart")
    @CommandCompletion("@nothing @nothing")
    public static void restart(Player player, Integer time, String reason) {
        if(RestartService.isRestarting()) {
            player.spigot().sendMessage(MineDown.parse("&cThe server is already restarting!"));
            return;
        }
        RestartService.restart(time, reason);
    }

    @Description("cancel a current restart")
    @Subcommand("cancel")
    @CommandPermission("silver.restart.cancel")
    @CommandCompletion("@nothing")
    public static void cancel(Player player) {
        if(!RestartService.isRestarting()) {
            player.spigot().sendMessage(MineDown.parse("&cThe server isn't restarting!"));
            return;
        }
        RestartService.cancelRestart();
    }

    @Description("evacuate the server")
    @Subcommand("evacuate")
    @CommandCompletion("@nothing")
    public static void evacuate(Player player) {
        if(!RestartService.isRestarting()) {
            player.spigot().sendMessage(MineDown.parse("&cThe server isn't restarting!"));
            return;
        }
        RestartService.evacuate(player);
    }
}
