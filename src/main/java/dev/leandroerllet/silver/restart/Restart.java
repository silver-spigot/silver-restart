package dev.leandroerllet.silver.restart;

import co.aikar.commands.BukkitCommandManager;
import dev.leandroerllet.silver.restart.command.RestartCommand;
import dev.leandroerllet.silver.restart.listener.PlayerLoginListener;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Restart extends JavaPlugin {

    @Getter
    private static Restart instance;


    @Override
    public void onLoad() {
        instance = this;
        saveDefaultConfig();
    }

    @Override
    public void onEnable() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        val manager = new BukkitCommandManager(this);
        manager.registerCommand(new RestartCommand());
        Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
